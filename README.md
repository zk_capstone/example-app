# ZKAuth Example app #

This is an example application to show the use of the ZKAuth library

## How to Run? ##
Start a mongoDB with default settings:

`mongod`

and run the application:

`mvn spring-boot:run`

go to: http://localhost:8080