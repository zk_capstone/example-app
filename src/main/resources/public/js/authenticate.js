(function (exports) {
    'use strict';

    window.DEBUG = false;
    var basepath = 'http://localhost:8080/';

    exports.onLogin = onLogin;
    exports.onRegister = onRegister;

    function onLogin() {
        var username = document.getElementById('username').value;
        var password = document.getElementById('password').value;
        var trusted = document.getElementById('trusted').checked;
        document.getElementById('spinner').style.display = 'block';
        document.getElementById('submitbutton').style.display = 'none';

        request('GET', 'salt/' + username, null, function () {
            if (this.readyState == 4 && this.status == 200) {
                ZKAuth.login(username, password, JSON.parse(this.responseText).salt, trusted, function (returnObject) {
                    request('POST', 'login', returnObject, function () {
                        if (this.readyState == 4) {
                            document.getElementById('spinner').style.display = 'none';
                            document.getElementById('submitbutton').style.display = 'block';
                            if (this.status == 200) {
                                showMessage('green', 'Login successful!');
                            } else {
                                showMessage('red', 'Login failed!');
                                ZKAuth.cleanCache(username);
                            }
                        }
                    });
                });
            }
        });
    }

    function onRegister() {
        var username = document.getElementById('username').value;
        var password = document.getElementById('password').value;
        var passwordCheck = document.getElementById('passwordcheck').value;
        if (password != passwordCheck) {
            showMessage('red', 'Passwords do not match!');
            return;
        }
        var trusted = document.getElementById('trusted').checked;
        document.getElementById('spinner').style.display = 'block';
        document.getElementById('submitbutton').style.display = 'none';

        ZKAuth.register(username, password, trusted, function (returnObject) {
            request('POST', 'register', returnObject, function () {
                if (this.readyState == 4) {
                    document.getElementById('spinner').style.display = 'none';
                    document.getElementById('submitbutton').style.display = 'block';
                    if (this.status == 200) {
                        showMessage('green', 'Registration successful!');
                    } else {
                        showMessage('red', 'Registration failed!');
                        ZKAuth.cleanCache(username);
                    }
                }
            });
        });
    }

    function showMessage(color, message) {
        var messageEl = document.getElementById('message');
        messageEl.style.color = color;
        messageEl.innerHTML = message;
    }

    function request(method, url, body, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = callback;
        request.open(method, basepath + url, true);
        request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify(body));
    }

})(typeof exports != 'undefined' ? exports : window);