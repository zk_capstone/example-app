package edu.jhu.isi.zkauth.exampleapp;

import edu.jhu.isi.zkauth.api.ZKAuth;
import edu.jhu.isi.zkauth.entities.Proof;
import edu.jhu.isi.zkauth.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AuthService {
    @Autowired
    private UserRepository repo;

    public void register(final User user) {
        final User dbUser = repo.findByUsername(user.getUsername());
        if (dbUser == null) {
            repo.save(user);
        } else {
            throw new IllegalArgumentException("This user already exists");
        }
    }

    public Boolean login(final String username, final Proof proof) {
        final User user = repo.findByUsername(username);
        return user != null && ZKAuth.login(user, proof);
    }

    public String getSalt(final String username) {
        final User user = repo.findByUsername(username);
        return user != null ? user.getSalt() : getRandomHexString(16);
    }

    private String getRandomHexString(final int length) {
        final Random r = new Random();
        String s = "";
        while (s.length() < length) {
            s += Integer.toHexString(r.nextInt());
        }

        return s.substring(0, length);
    }
}
