package edu.jhu.isi.zkauth.exampleapp;

import edu.jhu.isi.zkauth.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    User findByUsername(final String Username);
}
