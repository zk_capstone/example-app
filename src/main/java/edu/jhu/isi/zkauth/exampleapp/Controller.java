package edu.jhu.isi.zkauth.exampleapp;

import edu.jhu.isi.zkauth.entities.User;
import edu.jhu.isi.zkauth.exampleapp.dto.LoginDto;
import edu.jhu.isi.zkauth.exampleapp.dto.SaltDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @Autowired
    private AuthService authService;

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity register(@RequestBody final User user) {
        authService.register(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/salt/{username}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public
    @ResponseBody
    SaltDto getSalt(@PathVariable final String username) {
        return new SaltDto(authService.getSalt(username));
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity login(@RequestBody final LoginDto loginDto) {
        if (authService.login(loginDto.getUsername(), loginDto.getProof())) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }
}
