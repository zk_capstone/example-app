package edu.jhu.isi.zkauth.exampleapp.dto;

public class SaltDto {
    private String salt;

    public SaltDto(final String salt) {
        this.salt = salt;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(final String salt) {
        this.salt = salt;
    }
}
