package edu.jhu.isi.zkauth.exampleapp.dto;

import edu.jhu.isi.zkauth.entities.Proof;

public class LoginDto {
    private String username;
    private Proof proof;

    public String getUsername() {
        return username;
    }

    public Proof getProof() {
        return proof;
    }
}
